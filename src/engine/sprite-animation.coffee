import Behavior from './behavior'

defaultFps = 16

class SpriteAnimation extends Behavior
  frames: []

  fps: defaultFps

  millisPerFrame: 1000 / defaultFps

  isPlaying: false

  isLoop: false

  frameIdx: null

  prevFrameTime: null

  constructor: (gameObj, name, @frames = [], @fps = defaultFps) ->
    super gameObj, name
    @millisPerFrame = 1000 / @fps

  addFrame: (frame) ->
    @frames.push frame

  play: (startFrameIdx = 0) ->
    @isLoop = false
    @startAnimation startFrameIdx

  loop: (startFrameIdx = 0) ->
    @isLoop = true
    @startAnimation startFrameIdx

  startAnimation: (startFrameIdx = 0) ->
    @isPlaying = true
    @frameIdx = startFrameIdx

  stop: -> @isPlaying = false

  clear: ->
    @stop()
    @frameIdx = null

  update: ->
    if @isPlaying
      time = Date.now()
      if @prevFrameTime?
        elapsedTime = time - @prevFrameTime
        if elapsedTime >= @millisPerFrame
          @advanceFrame()
          @prevFrameTime += @millisPerFrame
      else @prevFrameTime = time
      @gameObj.sprite = @getCurrFrame()

  advanceFrame: ->
    @frameIdx =
      if @frameIdx >= @frames.length - 1
        if @isLoop then 0
        else
          @isPlaying = false
          @frameIdx
      else @frameIdx + 1

  getCurrFrame: -> if @frameIdx? then @frames[@frameIdx]

export default SpriteAnimation
