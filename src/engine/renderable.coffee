class Renderable
  # TODO: params?
  constructor: ->
    @color = { r: 1, g: 1, b: 1, a: 1 }
    @isEnabled = true

  # TODO: render only if is enabled
  render: ->

  hide: -> @isEnabled = false

  show: -> @isEnabled = true

  toggleEnabled: -> @isEnabled = not @isEnabled

export default Renderable