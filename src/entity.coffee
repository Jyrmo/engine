import { uniqueId } from 'lodash'

import Transform2D from './transform-2D'

childNamePrefix = 'ch_'
behaviorNamePrefix = 'beh_'

class Entity
   # TODO: possibly remove renderable param
  constructor: (@transform = new Transform2D(), @renderable = null) ->
    @children = new Map()
    @behaviors = new Map()
    @isEnabled = true

  setChild: (child, name = uniqueId childNamePrefix) ->
    @children.set name, child
    name

  removeChild: (name) -> @children.delete name

  clearChildren: -> @children.clear()

  setBehavior: (behavior, name = uniqueId behaviorNamePrefix) ->
    @behaviors.set name, behavior
    name

  removeBehavior: (name) -> @behaviors.delete name

  update: ->
    if @isEnabled
      @children.forEach (child) -> child.update()
      @behaviors.forEach (behavior) -> behavior.update()

  render: ->
    if @isEnabled
      # TODO: render children first and then parent or vice versa???
      if @renderable? then @renderable.render()
      @children.forEach (child) -> child.render()

  disable: -> @isEnabled = false

  enable: -> @isEnabled = true

  toggleEnabled: -> @isEnabled = not @isEnabled

export default Entity
