import { vec2 } from 'gl-matrix'

import { default as Transform2D } from '../transform-2D'
import { default as Entity } from '../entity'
import { default as GlyphEntity } from '../entity/glyph'

# TODO: multiline
# TODO: align left, right, center, justify
# TODO: word wrap
# TODO: letter spacing
# TODO: line spacing
# TODO: rotation

# TODO: account for multiline here as well
calculateTextWidth = (glyphs) ->
  width = 0
  for glyph in glyphs
    width += glyph.xAdvance
  width

calculateTextStartPos = (glyphs, pos, rotVec) ->
  width = calculateTextWidth glyphs
  scalar = -width / 2
  # TODO: naming
  backVec = [0, 0]
  vec2.scale backVec, rotVec, scalar
  startPos = [0, 0]
  vec2.add startPos, pos, backVec

# TODO: rn?
advanceGlyphPos = (glyphPos, rotVec, scalar) ->
  # TODO: cleanup/refactor
  additionVec = [0, 0]
  vec2.scale additionVec, rotVec, scalar
  vec2.add glyphPos, glyphPos, additionVec

# TODO: refer to font by name and get it from asset loader
# TODO: params as spec object
# TODO: pass in glyphs instead of font and str???
export default buildText = (str, font, pos = [0, 0], rot = 0) ->
  # TODO: refactor/cleanup
  # TODO: when rendering, consider transform ancestry
  transform = new Transform2D pos
  text = new Entity transform
  glyphs = font.getGlyphs str
  rotVec = [(Math.cos rot), (Math.sin rot)]
  glyphPos = calculateTextStartPos glyphs, pos, rotVec
  # TODO: factor out rot as v2
  for glyph in glyphs
    # TODO: clone
    currGlyphPos = [glyphPos[0], glyphPos[1]]
    glyphTransform = new Transform2D currGlyphPos, [1, 1], rot
    glyphEntity = new GlyphEntity glyphTransform, glyph
    text.setChild glyphEntity
    advanceGlyphPos glyphPos, rotVec, glyph.xAdvance
  text
