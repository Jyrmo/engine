tickTime = null
deltaTime = null

# TODO: refactor

export updateTime = (time = performance.now()) ->
  if tickTime? then deltaTime = time - tickTime
  tickTime = time

export getTickTime = -> tickTime

export time = getTickTime

export t = time

export getDeltaTime = -> deltaTime

export getDt = getDeltaTime

export dt = getDt

export getRealTime = performance.now

export realTime = getRealTime

export rt = realTime