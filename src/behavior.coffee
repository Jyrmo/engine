class Behavior
  constructor: (@entity, name) ->
    @entity.setBehavior this, name
    @start()

  start: ->

  update: ->

export default Behavior
