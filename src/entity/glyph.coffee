import { GlyphRenderable } from '@jyrmo/renderer'

import { default as Entity } from '../entity'

class Glyph extends Entity
  constructor: (transform, glyph, tint) ->
    super transform
    new GlyphRenderable this, glyph, tint

export default Glyph
