import * as renderer from '@jyrmo/renderer'

import { updateTime } from './time'

# TODO: gameloop should not depend on renderer?

defaultFps = 60

class GameLoop
  # TODO: should we keep the scene here or perhaps have a separate container?
  constructor: (@scene, @fps = defaultFps) ->
    @mpf = 1000 / @fps
    @isPlaying = false

  start: ->
    # TODO: scene.preload?????
    @lag = 0.0
    @isPlaying = true
    updateTime()
    requestAnimationFrame @render

  # TODO: visualize the render and update times
  render: (time) =>
    if @isPlaying
      requestAnimationFrame @render
      if @currRenderTime? then @prevRenderTime = @currRenderTime
      @currRenderTime = time
      if @prevRenderTime? then @lag += @currRenderTime - @prevRenderTime
      @update time
      renderer.clear()
      @scene.render()
    # TODO: else @scene.unload()????

  update: ->
    while @lag >= @mpf and @isPlaying
      updateTime()
      @scene.update()
      @lag -= @mpf

  stop: => @isPlaying = false

export default GameLoop
