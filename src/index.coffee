# TODO: a more elegant solution for this
import * as assetLoader from '@jyrmo/asset-loader'
import * as renderer from '@jyrmo/renderer'

import * as time from './time'
import * as build from './build'

export { assetLoader, renderer, time, build }
export { default as GameLoop } from './game-loop'
export { default as Transform2D } from './transform-2D' 
export { default as Entity } from './entity'
# TODO: entities container?
export { default as GlyphEntity } from './entity/glyph'
export { default as Behavior } from './behavior'

# TODO: export transform 2D
# TODO: Rework input handling
# TODO: an engine.init function?
# TODO: text entity type
# TODO: rm http-server dependency

# TODO: sprite map asset loader
# TODO: remove old unnecessary stuff from engine dir
# TODO: web assembly
# TODO: think through the whole bundling thing