require('babel-polyfill')

const engine = require('../lib/index')

const Animate = require('./animate')

const { build, renderer, Entity, GameLoop } = engine
const loader = engine.assetLoader

const { SpriteRenderable } = renderer

const imgSpec = {
  type: 'image',
  name: 'arrow-up',
  params: {
    src: 'kb-arrow-up.png'
  }
}

const fontSpec = {
  type: 'font',
  name: 'consolas-72',
  params: {
    fntUrl: 'Consolas-72.fnt',
    pngUrl: 'Consolas-72.png'
  }
}

const buildArrow = () => {
  const arrow = new Entity()
  arrow.transform.scale = [96, 96]
  const arrowSprite = {
    img: loader.assets.image['arrow-up'],
    x: 0,
    y: 0,
    width: 96,
    height: 96
  }
  new SpriteRenderable(arrow, arrowSprite)
  new Animate(arrow, 'animate')

  return arrow
}

// TODO: the y baseline is messed up for low glyphs
// TODO: push async canvas loading stuff to renderer???

window.onload = () => {
  loader.loadSpecs([imgSpec, fontSpec]).then(() => {
    const canvas = document.getElementById('canvas')
    const shaderSpec = { builtIn: 'sprite' }
    const resolution = [800, 600]
    renderer.init({canvas, shader: shaderSpec, resolution})

    const scene = new Entity()
    const arrow = buildArrow()
    scene.setChild(arrow, 'arrow')

    const str = 'Textp entity'
    const textEntity = build.text(str, loader.assets.font['consolas-72'], [0, 0], 0.1)
    scene.setChild(textEntity, 'textEntity')

    const gameLoop = new GameLoop(scene)
    setTimeout(gameLoop.stop, 5000)
    gameLoop.start()

    window.stop = gameLoop.stop
  })
}
