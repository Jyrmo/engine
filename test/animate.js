const { Behavior, time } = require('../lib/index')

class Animate extends Behavior {
  update() {
    const t = time.t()
    const x = 200 * Math.sin(t / 700)
    const y = 150 * Math.cos(t / 600)
    this.entity.transform.pos = [x, y]
  }
}

module.exports = Animate