export { default as util } from '@jyrmo/util'

export { t, dt } from './time'
export { default } from './game-loop'
export { default as Entity } from './entity'
export { default as Behavior } from './behavior'

# TODO: Shorthand sprite entity
# TODO: Shorthand glyph entity
# TODO: Rework input handling
# TODO: an engine.init function?
# TODO: text entity type

# TODO: remove old unnecessary stuff from engine dir
# TODO: web assembly
# TODO: think through the whole bundling thing